# Menu Link Destination

Allows a site builder to configure menu links to automatically receive a
`destination` query parameter pointing:
* Either to the `destination` query parameter from the current page URL, if any,
* Or to the current page URL if the URL contains no `destination` parameter.

## Usage

Menu links defined in YAML files that should receive a `destination` query
parameter, should add a `destination: true` property:

```yaml
my_menu.link:
  title: 'My link'
  route_name: 'some.route'
  menu_name: menu
  destination: true
```

Links created with Menu Link Content (`menu_link_content`) module are also able
to expose the destination as a query parameter. In the link add/edit form, a new
`Add a destination query parameter` checkbox allows to automatically add the
destination with the same effect as for YAML defined menu links.
