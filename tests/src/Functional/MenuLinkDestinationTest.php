<?php

declare(strict_types=1);

namespace Drupal\Tests\menu_link_destination\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\system\Entity\Menu;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests the Menu Link Destination module functionality.
 *
 * @group menu_link_destination
 */
class MenuLinkDestinationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'contact_test',
    'menu_link_destination',
    'menu_link_destination_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test module functionality.
   */
  public function testLinks(): void {
    Role::load(AccountInterface::ANONYMOUS_ROLE)->grantPermission('access site-wide contact form')->save();
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();
    Menu::create([
      'id' => 'test_menu',
      'label' => 'Test menu',
    ])->save();
    $this->placeBlock('system_menu_block:test_menu');

    // Link without a 'destination' key and current URL without 'destination'.
    $this->drupalGet('/user/login');
    $page->clickLink('Link without destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/menu_link_destination_form_test');

    // Link without a 'destination' key and current URL with 'destination'.
    $this->drupalGet('/user/login', ['query' => ['destination' => 'contact']]);
    $page->clickLink('Link without destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/menu_link_destination_form_test');

    // Link with a 'destination' key and current URL without 'destination'.
    $this->drupalGet('/user/login');
    $page->clickLink('Link with destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/user/login');

    // Link with a 'destination' key and current URL with 'destination'.
    $this->drupalGet('/user/login', ['query' => ['destination' => 'contact']]);
    $page->clickLink('Link with destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/contact');

    // Link with a 'destination' key and current URL without 'destination' but
    // with a static destination already set.
    $this->drupalGet('/user/login');
    $page->clickLink('Link with static destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/some/path');

    // Link with a 'destination' key and current URL with 'destination' but with
    // a static destination already set.
    $this->drupalGet('/user/login', ['query' => ['destination' => 'contact']]);
    $page->clickLink('Link with static destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/some/path');
  }

  /**
   * Checks case when URL is not defined.
   */
  public function testInvalidLink(): void {
    $invalidLink = [
      '#type' => 'link',
      '#title' => 'test',
    ];
    $linkOutput = \Drupal::service('renderer')->renderRoot($invalidLink);
    $this->assertEquals('', $linkOutput);
  }

}
