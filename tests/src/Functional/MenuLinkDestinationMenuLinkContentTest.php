<?php

declare(strict_types=1);

namespace Drupal\Tests\menu_link_destination\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\system\Entity\Menu;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests Menu Link Destination with Menu Link Content links.
 *
 * @group menu_link_destination
 */
class MenuLinkDestinationMenuLinkContentTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'contact_test',
    'menu_link_content',
    'menu_link_destination',
    'menu_link_destination_form_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests Menu Link Destination with Menu Link Content links.
   */
  public function testMenuLinkContent(): void {
    Role::load(AccountInterface::ANONYMOUS_ROLE)->grantPermission('access site-wide contact form')->save();
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();
    Menu::create([
      'id' => 'test_menu',
      'label' => 'Test menu',
    ])->save();
    MenuLinkContent::create([
      'title' => 'Link without destination',
      'menu_name' => 'test_menu',
      'link' => [
        'uri' => 'route:menu_link_destination_form_test.form',
        'options' => [
          'menu_link_destination' => FALSE,
        ],
      ],
    ])->save();
    MenuLinkContent::create([
      'title' => 'Link with destination',
      'menu_name' => 'test_menu',
      'link' => [
        'uri' => 'route:menu_link_destination_form_test.form',
        'options' => [
          'menu_link_destination' => TRUE,
        ],
      ],
    ])->save();
    MenuLinkContent::create([
      'title' => 'Link with static destination',
      'menu_name' => 'test_menu',
      'link' => [
        'uri' => 'route:menu_link_destination_form_test.form',
        'options' => [
          'query' => [
            'destination' => 'some/path',
          ],
          'menu_link_destination' => TRUE,
        ],
      ],
    ])->save();
    $this->placeBlock('system_menu_block:test_menu');

    // Link without a 'destination' key and current URL without 'destination'.
    $this->drupalGet('/user/login');
    $page->clickLink('Link without destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/menu_link_destination_form_test');

    // Link without a 'destination' key and current URL with 'destination'.
    $this->drupalGet('/user/login', ['query' => ['destination' => 'contact']]);
    $page->clickLink('Link without destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/menu_link_destination_form_test');

    // Link with a 'destination' key and current URL without 'destination'.
    $this->drupalGet('/user/login');
    $page->clickLink('Link with destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/user/login');

    // Link with a 'destination' key and current URL with 'destination'.
    $this->drupalGet('/user/login', ['query' => ['destination' => 'contact']]);
    $page->clickLink('Link with destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/contact');

    // Link with a 'destination' key and current URL without 'destination' but
    // with a static destination already set.
    $this->drupalGet('/user/login');
    $page->clickLink('Link with static destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/some/path');

    // Link with a 'destination' key and current URL with 'destination' but with
    // a static destination already set.
    $this->drupalGet('/user/login', ['query' => ['destination' => 'contact']]);
    $page->clickLink('Link with static destination');
    $page->pressButton('Submit');
    $assert->pageTextContains('Form submitted');
    $assert->addressEquals('/some/path');
  }

}
