<?php

declare(strict_types=1);

namespace Drupal\menu_link_destination;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;

/**
 * Provides a pre-render callback for links.
 */
class MenuLinkDestination implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['preRenderLink'];
  }

  /**
   * Adds cache metadata to menu links with destination.
   *
   * @param array $element
   *   The link element.
   *
   * @return array
   *   The returning link element.
   */
  public static function preRenderLink(array $element): array {
    $url = $element['#url'] ?? NULL;

    if ($url instanceof Url && !empty($url->getOption('menu_link_destination'))) {
      (new CacheableMetadata())
        // Such links cache is varying by URL.
        ->setCacheContexts(['url'])
        ->applyTo($element);
    }

    return $element;
  }

}
